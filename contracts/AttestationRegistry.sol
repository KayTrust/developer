pragma solidity ^0.4.24;

contract AttestationRegistry {
  event Verified(bytes32 indexed hash, address attester, uint256 iat, uint256 exp);

  struct Attestation {
    // Attestation date (0 means "not verified")
    uint iat;
    // Attestation expiration date (0 means "never expires")
    uint exp;
  }

  // hash => attester => Verification
  mapping (bytes32 => mapping (address => Attestation)) public attestations;

  function attest(bytes32 hash, uint iat, uint exp) public {
    attestations[hash][msg.sender] = Attestation(iat, exp);
    emit Verified(hash, msg.sender, iat, exp);
  }
}
